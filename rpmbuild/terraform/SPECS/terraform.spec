%define  debug_package %{nil}
Name:		terraform
Version:	0.12.9
Release:	1%{?dist}
Summary:	HashiCorp Terraform enables you to safely and predictably create, change, and improve infrastructure.

Group:		Applications
License:	N/A
#URL:		
Source0:	https://releases.hashicorp.com/terraform/%{version}/%{name}_%{version}_linux_amd64.zip


#BuildRequires:	
#Requires:	

%description
HashiCorp Terraform enables you to safely and predictably create, change, and improve infrastructure. 
It is an open source tool that codifies APIs into declarative configuration files that can be shared 
amongst team members, treated as code, edited, reviewed, and versioned.

%prep
%setup -c %{name}-%{version}


%build
#cp -a  * %{buildroot}/usr/share/%{name}-%{version}

%install
install -m 0750 -d  %{buildroot}/usr/share/%{name}-%{version}
install -m 0755 terraform %{buildroot}/usr/share/%{name}-%{version}/

%files
%attr(755,-,-) /usr/share/%{name}-%{version}/
/usr/share/%{name}-%{version}/%{name}


%post
#!/bin/bash
VERSION="%{version}"
PRIORITY="${VERSION//[.0]/}"
echo $VERSION
echo $PRIORITY
alternatives --install /usr/local/bin/%{name} %{name} /usr/share/%{name}-%{version}/%{name} ${PRIORITY}
exit 0

%postun
   alternatives --remove %{name} /usr/share/%{name}-%{version}/%{name}
exit 0


%doc



%changelog
