%define __jar_repack 		%{nil}
%define __python 		%{nil}
%define __strip 		%{nil}
%define debug_package %{nil}
Name:		redis
Version:	5.0.5
Release:	1%{?dist}
Summary:	Redis is an open source (BSD licensed), in-memory data structure store, used as a database, cache and message broker. It supports data structures such as strings, hashes, lists, sets, sorted sets with range queries, bitmaps, hyperloglogs, geospatial indexes with radius queries and streams. Redis has built-in replication, Lua scripting, LRU eviction, transactions and different levels of on-disk persistence, and provides high availability via Redis Sentinel and automatic partitioning with Redis Cluster

Group:		Application
License:	N/A
URL:		http://redis.io
Source0:	http://download.redis.io/releases/redis-%{version}.tar.gz
AutoProv:   0
AutoReq: 	0
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-XXXXXX)
#BuildRequires:	
Requires: 	initscripts
Prefix:         /opt
BuildArch:      noarch

%description
Redis installer

%prep
%setup -q


%build
make

%pre
/usr/bin/getent group %{name} > /dev/null || /usr/sbin/groupadd -r %{name}
/usr/bin/getent passwd %{name} > /dev/null || /usr/sbin/useradd -r -g %{name} -d /home/%{name} -s /sbin/nologin -c "Redis service account" %{name}


%install
mkdir -p %{buildroot}/usr/local/bin/
mkdir -p %{buildroot}/etc/%{name}
mkdir -p %{buildroot}/usr/lib/systemd/system/

cp src/redis-server %{buildroot}/usr/local/bin/
cp src/redis-sentinel %{buildroot}/usr/local/bin/
cp src/redis-benchmark %{buildroot}/usr/local/bin/
cp src/redis-check-rdb %{buildroot}/usr/local/bin/
cp src/redis-check-aof %{buildroot}/usr/local/bin/
cp src/redis-cli %{buildroot}/usr/local/bin/
cp redis.conf %{buildroot}/etc/%{name}/

cat << 'EOF' > %{buildroot}/usr/lib/systemd/system/%{name}.service
[Unit]
Description=Redis In-Memory Data Store
After=network.target

[Service]
Type=notify
ExecStart=/usr/local/bin/redis-server /etc/redis/redis.conf --supervised systemd --daemonize no
ExecStop=/usr/local/bin/redis-cli shutdown
ExecReload=/bin/kill -USR2 $MAINPID
Restart=always

[Install]
WantedBy=multi-user.target
EOF

%files
%defattr(750,%{name},%{name},750)
/usr/local/bin/redis-server
/usr/local/bin/redis-sentinel
/usr/local/bin/redis-benchmark
/usr/local/bin/redis-check-rdb
/usr/local/bin/redis-check-aof
/usr/local/bin/redis-cli
%attr(640,-,-) /etc/%{name}/redis.conf
/usr/lib/systemd/system/%{name}.service

