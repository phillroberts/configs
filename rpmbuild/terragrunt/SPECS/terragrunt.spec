%define  debug_package %{nil}
Name:		terragrunt
Version:	0.14.7
Release:	1%{?dist}
Summary:	Terragrunt is a thin wrapper for Terraform that provides extra tools for working with multiple Terraform modules.

Group:		Applications
License:	N/A
#URL:		
Source0:        https://github.com/gruntwork-io/%{name}/releases/download/v%{version}/%{name}_linux_amd64


#BuildRequires:	
#Requires:	

%description
Terragrunt is a thin wrapper for Terraform that provides extra tools for keeping your 
Terraform configurations DRY, working with multiple Terraform modules, and managing remote state.

%prep
#mv %{SOURCE0} %{SOURCE0}.zip
#%setup -c %{name}-%{version}


%build

%install
install -m 0750 -d  %{buildroot}/usr/share/%{name}-%{version}
install -m 0755 %{SOURCE0} %{buildroot}/usr/share/%{name}-%{version}/%{name}

%files
%attr(755,-,-) /usr/share/%{name}-%{version}/
/usr/share/%{name}-%{version}/%{name}


%post
#!/bin/bash
VERSION="%{version}"
PRIORITY="${VERSION//./}"
echo $VERSION
echo $PRIORITY
alternatives --install /usr/local/bin/%{name} %{name} /usr/share/%{name}-%{version}/%{name} ${PRIORITY}
exit 0

%postun
   alternatives --remove %{name} /usr/share/%{name}-%{version}/%{name}
exit 0


%doc



%changelog
