%define __jar_repack 		%{nil}
%define __python 		%{nil}
%define __strip 		%{nil}
Name:		geoserver
Version:	2.11.1
Release:	1%{?dist}
Summary:	Geoserver

Group:		Application
License:	N/A
URL:		http://geoserver.org
Source0:	geoserver-2.11.1.zip
AutoProv:       0
AutoReq: 	0
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-XXXXXX)
#BuildRequires:	
Requires: 	initscripts
Prefix:         /opt
BuildArch:      noarch

%description
Geoserver installer

%prep
%setup -q


%build

%pre
/usr/bin/getent group %{name} > /dev/null || /usr/sbin/groupadd -r %{name}
/usr/bin/getent passwd %{name} > /dev/null || /usr/sbin/useradd -r -g %{name} -d /home/%{name} -s /sbin/nologin -c "Geoserver service account" %{name}


%install

install -m 0750 -d %{buildroot}/opt/%{name}-%{version}
cp -a  * %{buildroot}/opt/%{name}-%{version}


%files
%defattr(750,%{name},%{name},750)
/opt/%{name}-%{version}/bin/startup.sh
/opt/%{name}-%{version}/bin/shutdown.sh
%attr(440,-,-) /opt/%{name}-%{version}/bin/startup.bat
%attr(440,-,-) /opt/%{name}-%{version}/bin/shutdown.bat

%defattr(640,%{name},%{name},750)
%dir /opt/%{name}-%{version}
%dir /opt/%{name}-%{version}/bin
%dir /opt/%{name}-%{version}/data_dir
%dir /opt/%{name}-%{version}/etc
%dir /opt/%{name}-%{version}/lib
%dir /opt/%{name}-%{version}/logs
%dir /opt/%{name}-%{version}/modules
%dir /opt/%{name}-%{version}/resources
%dir /opt/%{name}-%{version}/webapps
/opt/%{name}-%{version}/data_dir/*
/opt/%{name}-%{version}/etc/*
/opt/%{name}-%{version}/lib/*
/opt/%{name}-%{version}/logs/*
/opt/%{name}-%{version}/modules/*
/opt/%{name}-%{version}/resources/*
/opt/%{name}-%{version}/webapps/*
/opt/%{name}-%{version}/GPL.txt
/opt/%{name}-%{version}/LICENSE.txt
/opt/%{name}-%{version}/README.txt
/opt/%{name}-%{version}/RELEASE_NOTES.txt
/opt/%{name}-%{version}/RUNNING.txt
/opt/%{name}-%{version}/start.ini
/opt/%{name}-%{version}/start.jar
/opt/%{name}-%{version}/VERSION.txt




%post
#!/bin/bash
[ -h /opt/%{name} ] || /bin/ln -s /opt/%{name}-%{version} /opt/%{name}

%postun
#!/bin/bash
if [ "$1" == 0 ]; then
    [[ -h /opt/%{name} ]] && /bin/unlink /opt/%{name}
    /usr/sbin/userdel %{name}
    exit 0
fi


%doc



%changelog
* July 19, 2017 Phillip Roberts <proberts@corelution.com>
- Initial creation of a geoserver rpm

