Name:		tmux
Version:	2.5
Release:	1%{?dist}
Summary:	tmux terminal emulator

Group:		Applications
License:	N/A
#URL:		
Source0:	https://github.com/tmux/tmux/releases/download/%{version}/%{name}-%{version}.tar.gz


BuildRequires:	gcc kernel-devel make ncurses-devel
Requires:	libevent

%description


%prep
%setup -q

%build
%configure
LDFLAGS="-L/usr/local/lib -Wl,-rpath=/usr/local/lib" ./configure --prefix=/usr/local
make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}
echo "--------------------%{buildroot}"

mkdir -p $RPM_BUILD_ROOT/usr/share/icons/
mkdir -p $RPM_BUILD_ROOT/usr/share/applications
cp %{_sourcedir}/tmux.png %{buildroot}/usr/share/icons/
cp %{_sourcedir}/tmux.desktop %{buildroot}/usr/share/applications/
cp %{_sourcedir}/tmux-attach %{buildroot}/usr/local/bin/

%files
/usr/local/bin/tmux
%attr(755,-,-) /usr/local/bin/tmux-attach
/usr/local/share/man/man1/tmux.1
/usr/share/icons/tmux.png
/usr/share/applications/tmux.desktop

%doc



%changelog

