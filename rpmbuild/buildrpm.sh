#!/bin/bash
DIRNAME=`dirname $1`
SPECFILE=`basename $1`
pushd $DIRNAME
cd ..
rpmbuild --undefine=_disable_source_fetch --define "_topdir $PWD" -bb "SPECS/${SPECFILE}"
popd
