#!/bin/bash
# This script adds additional misc bash aliases

# sort the current directory by size
alias ds='du -scBM * | sort -n'
