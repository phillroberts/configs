#!/bin/bash
# This script adds additional git aliases

alias gcof='function _cof() { git checkout "feature/$1"; }; _cof'
alias gcod='git checkout develop'
alias gcom='git checkout master'
