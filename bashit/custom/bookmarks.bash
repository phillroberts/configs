#!/bin/bash
# This script overrides the default bookmarks function located
# in bash_it/plugins/available/dirs.plugin.bash



# remove the L alias and replace it with 
# a custom function with filter capability
unalias L
L () {
	about 'List saved bookmarks'
	param '1: bookmark name'
	example '$ L home - shows just the home bookmark'
	example '$ L - shows all bookmarks'
	group 'dirs'
    
	[[ $# -eq 1 ]] && { grep "$@=" ~/.dirs; return 0; }
	
	cat ~/.dirs | sort

}

H () {
	about 'display help information on the bookmarks feature'	
	group 'dirs'
	glossary dirs
}

S () {
    about 'save a bookmark'
    param '1: bookmark name'
    example '$ S mybkmrk'
    group 'dirs'

    [[ $# -eq 1 ]] || { echo "${FUNCNAME[0]} function requires 1 argument"; return 1; }
  
    sed -i "/^$@=/d" ~/.dirs;
    echo "$@"=\"`pwd`\" >> ~/.dirs;
    source ~/.dirs ;
}
